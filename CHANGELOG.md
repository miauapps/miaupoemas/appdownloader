- 1812.1
    - Primera subida de app
    
- 1812.2
    - Se sube version 1812.3 de app

- 1812.3
    - Se modifica servicio web y se sube version 1812.4 de app
    
- 1812.4
    - Se sube version 1812.4.1 de app
    
- 1812.5
    - Se sube version 1812.4.2 de app
    
- 1812.6
    - Se sube version 1812.5 de app
    
- 1812.7
    - Se sube version 1812.5.1 de app
    
- 1812.8
    - Se sube version 1812.6 de app
    
- 1812.9
    - Se sube version 1812.7 de app
    
- 1812.10
    - Se sube version 1812.8 de app
    
- 1812.11
    - Se sube version 1812.9 de app
    
- 1812.12
    - Se sube version 1812.10 de app
    - Se modifica script newapp.sh
