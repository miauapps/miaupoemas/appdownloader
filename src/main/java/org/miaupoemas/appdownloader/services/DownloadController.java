package org.miaupoemas.appdownloader.services;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

@RestController
public class DownloadController {

    private String getJarPath() {
        String original = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        original = original.substring(5);

        if (original.contains("!"))
            return original.split("!")[0];
        else
            return original;
    }

    @RequestMapping(path = "/app.apk", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] downloadApp() {
        String jarPath = getJarPath();
        try {
            JarInputStream inputStream = new JarInputStream(new FileInputStream(jarPath));

            JarEntry entry;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer;
            int read;

            int entrySize;
            while ((entry = inputStream.getNextJarEntry()) != null) {
                if (entry.getName().contains("app.apk")) {
                    entrySize = (int) entry.getSize();
                    buffer = new byte[entrySize>0?entrySize:102400];
                    while ((read = inputStream.read(buffer)) != -1)
                        baos.write(buffer, 0, read);
                    break;
                }
            }
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
