package org.miaupoemas.appdownloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDownloaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppDownloaderApplication.class, args);
    }
}
